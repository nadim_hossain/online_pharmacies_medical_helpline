-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 03, 2022 at 06:23 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_pharmacies`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` blob DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `name`, `email`, `gender`, `image`, `number`) VALUES
(1, 'Nadim99', 'Nadim!1155', 'Nadim Ahmed', 'nadim4995@gmail.com', 'male', '', '01852814232');

-- --------------------------------------------------------

--
-- Table structure for table `buyers`
--

CREATE TABLE `buyers` (
  `buyerid` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` blob DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buyers`
--

INSERT INTO `buyers` (`buyerid`, `username`, `password`, `name`, `address`, `gender`, `status`, `image`, `number`, `adminid`, `created_at`, `updated_at`) VALUES
(1, 'mohok909', 'mohok9o9@@', 'Mohok', 'kuratoli', 'male', 'active', '', '01767268796', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(2, 'sufian', 'sufian9o9@@', 'Sufian', 'kuratoli', 'male', 'active', '', '01763456780', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(3, 'saidul', 'saidul9o9@@', 'Saidul', 'kuratoli', 'male', 'active', '', '01741035680', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(4, 'ratul', 'ratul9o9@@', 'Ratul', 'kuratoli', 'male', 'active', '', '01743862780', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(5, 'nayeem', 'nayeem9o9@@', 'Nayeem', 'kuratoli', 'female', 'inactive', '', '01862124870', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE `channels` (
  `channelid` bigint(20) UNSIGNED NOT NULL,
  `channelname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `channeltype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bankaccountnumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminid` bigint(20) UNSIGNED NOT NULL,
  `sellerid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`channelid`, `channelname`, `channeltype`, `bankaccountnumber`, `adminid`, `sellerid`, `created_at`, `updated_at`) VALUES
(1, 'Icenter', 'Phone', '75812581825', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(2, 'FitnessBlender', 'Health & Fitness', '96751235472', 1, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(3, 'Love Sweat Fitness', 'Health & Fitness', '74536985120', 1, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `value` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`id`, `code`, `date`, `value`, `created_at`, `updated_at`) VALUES
(1, 'ABC123', '2022-06-30', 30, '2022-07-02 10:02:46', '2022-07-02 10:02:46'),
(2, 'DEF456', '2022-07-20', 30, '2022-07-02 10:02:46', '2022-07-02 10:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `deliveris`
--

CREATE TABLE `deliveris` (
  `deliveriid` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` blob DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productid` bigint(20) UNSIGNED NOT NULL,
  `orderid` bigint(20) UNSIGNED NOT NULL,
  `adminid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deliveris`
--

INSERT INTO `deliveris` (`deliveriid`, `username`, `password`, `name`, `address`, `gender`, `status`, `image`, `number`, `salary`, `productid`, `orderid`, `adminid`, `created_at`, `updated_at`) VALUES
(1, 'Sumit', 'Sumit!1155', 'Sumit', 'Kuratoli', 'female', 'active', '', '01767268796', '3000', 1, 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(188, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(189, '2022_06_19_185927_create_admins_table', 1),
(190, '2022_06_21_162004_create_sellers_table', 1),
(191, '2022_06_21_162626_create_channels_table', 1),
(192, '2022_06_21_162735_create_products_table', 1),
(193, '2022_06_21_162830_create_buyers_table', 1),
(194, '2022_06_21_164657_create_payments_table', 1),
(195, '2022_06_21_164747_create_orders_table', 1),
(196, '2022_06_21_164848_create_odetails_table', 1),
(197, '2022_06_21_165009_create_deliveris_table', 1),
(198, '2022_06_21_165217_create_pdetails_table', 1),
(199, '2022_06_21_165306_create_shoppingcarts_table', 1),
(200, '2022_06_27_091016_create_coupons_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `odetails`
--

CREATE TABLE `odetails` (
  `ordersdetailsid` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` blob DEFAULT NULL,
  `productid` bigint(20) UNSIGNED NOT NULL,
  `orderid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `odetails`
--

INSERT INTO `odetails` (`ordersdetailsid`, `description`, `image`, `productid`, `orderid`, `created_at`, `updated_at`) VALUES
(1, 'Valo Phone', '', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderid` bigint(20) UNSIGNED NOT NULL,
  `productname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tprice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `buyerid` bigint(20) UNSIGNED NOT NULL,
  `productid` bigint(20) UNSIGNED NOT NULL,
  `channelid` bigint(20) UNSIGNED NOT NULL,
  `paymentid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderid`, `productname`, `quantity`, `price`, `tprice`, `status`, `date`, `buyerid`, `productid`, `channelid`, `paymentid`, `created_at`, `updated_at`) VALUES
(1, 'Abacavir', '3', '120000', '360000', 'Done', '2022-07-02 00:00:00', 1, 1, 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(2, 'Adrenaclick epinephrine', '4', '20000', '80000', 'Done', '2022-07-02 00:00:00', 3, 2, 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(3, 'Advil (ibuprofen)', '5', '100000', '500000', 'Done', '2022-07-02 00:00:00', 1, 3, 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(4, 'Elase (fibrinolysin w/dnase-topical ointment)', '5', '90000', '450000', 'pending', '2022-07-02 00:00:00', 1, 4, 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(5, 'Emtriva emtricitabine', '5', '100', '500', 'Done', '2022-07-02 00:00:00', 2, 8, 2, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(6, 'Erleada apalutamide', '50', '950', '47500', 'Done', '2022-07-02 00:00:00', 1, 9, 2, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(7, 'Etravirine Intelence', '100', '700', '70000', 'pending', '2022-07-02 00:00:00', 2, 10, 2, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(8, 'Ketoconazole vs Selenium Sulfide', '100', '150', '15000', 'Done', '2022-07-02 00:00:00', 3, 12, 3, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(9, 'Kytril (granisetron-injection)', '80', '600', '48000', 'Done', '2022-07-02 00:00:00', 1, 14, 3, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(10, 'Uni-Ease (docusate-oral)', '50', '1000', '50000', 'pending', '2022-07-02 00:00:00', 3, 15, 3, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `channelname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bankaccountnumber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminid` bigint(20) UNSIGNED NOT NULL,
  `channelid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `channelname`, `amount`, `bankaccountnumber`, `adminid`, `channelid`, `created_at`, `updated_at`) VALUES
(1, 'Icenter', '9000', '75812581825', 1, 1, '2022-07-02 16:02:46', '2022-07-03 04:00:26'),
(2, 'FitnessBlender', '5800', '96751235472', 1, 2, '2022-07-02 16:02:46', '2022-07-03 03:57:16'),
(3, 'Love Sweat Fitness', '100', '74536985120', 1, 3, '2022-07-02 16:02:46', '2022-07-02 11:41:37');

-- --------------------------------------------------------

--
-- Table structure for table `pdetails`
--

CREATE TABLE `pdetails` (
  `pdetailid` bigint(20) UNSIGNED NOT NULL,
  `productname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` blob DEFAULT NULL,
  `productid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pdetails`
--

INSERT INTO `pdetails` (`pdetailid`, `productname`, `price`, `description`, `quantity`, `image`, `productid`, `created_at`, `updated_at`) VALUES
(1, 'Iphone', '120000', '1', '1', '', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productid` bigint(20) UNSIGNED NOT NULL,
  `productname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tprice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `image` blob DEFAULT NULL,
  `adminid` bigint(20) UNSIGNED NOT NULL,
  `channelid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productid`, `productname`, `price`, `quantity`, `tprice`, `date`, `image`, `adminid`, `channelid`, `created_at`, `updated_at`) VALUES
(1, 'Abacavir', '120000', '100', '120000000', '2022-07-02 00:00:00', '', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(2, 'Adrenaclick epinephrine', '40000', '120', '4800000', '2022-07-02 00:00:00', '', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(3, 'Advil (ibuprofen)', '20000', '50', '1000000', '2022-07-02 00:00:00', '', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(4, 'efinaconazole Jublia', '100000', '150', '15000000', '2022-07-02 00:00:00', '', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(5, 'Elase (fibrinolysin w/dnase-topical ointment)', '90000', '40', '3600000', '2022-07-02 00:00:00', '', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(6, 'elbasvir_and_grazoprevir', '900', '100', '90000', '2022-07-02 00:00:00', '', 1, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(7, 'Eminase (anistreplase-injection)', '500', '600', '300000', '2022-07-02 00:00:00', '', 1, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(8, 'Emtriva emtricitabine', '100', '600', '60000', '2022-07-02 00:00:00', '', 1, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(9, 'Erleada apalutamide', '950', '90', '85500', '2022-07-02 00:00:00', '', 1, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(10, 'Etravirine Intelence', '700', '500', '350000', '2022-07-02 00:00:00', '', 1, 2, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(11, 'Kaopectate Advanced Formula (attapulgite)', '400', '500', '200000', '2022-07-02 00:00:00', '', 1, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(12, 'Ketoconazole vs Selenium Sulfide', '150', '600', '90000', '2022-07-02 00:00:00', '', 1, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(13, 'Klisyri tirbanibulin', '300', '100', '30000', '2022-07-02 00:00:00', '', 1, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(14, 'Kytril (granisetron-injection)', '600', '900', '540000', '2022-07-02 00:00:00', '', 1, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(15, 'Uni-Ease (docusate-oral)', '1000', '500', '500000', '2022-07-02 00:00:00', '', 1, 3, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `sellerid` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` blob DEFAULT NULL,
  `number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `adminid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`sellerid`, `username`, `password`, `name`, `address`, `status`, `image`, `number`, `adminid`, `created_at`, `updated_at`) VALUES
(1, 'mohok909', 'MOHOK909@', 'Mahadial Mohok Million', 'mahadialmohok3@gmail.com', 'active', '', '01767268796', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(2, 'Sufian9', 'Sufian9@', 'Sufina Hossain', 'Sufina3@gmail.com', 'active', '', '01760961796', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(3, 'Sumit10', 'Sumit10@', 'Sumit Hossain', 'Sumit10@gmail.com', 'active', '', '01960981796', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(4, 'Nadim099', 'Nadim099@', 'Nadim Hossain', 'Nadim099@gmail.com', 'active', '', '01960916596', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(5, 'Nayeem099', 'Nayeem099@', 'Nayeem Hossain', 'Nayeem099@gmail.com', 'inactive', '', '01960916596', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46'),
(6, 'Ratul143', 'Ratul143@', 'Ratul Hossain', 'Ratul143@gmail.com', 'active', '', '0136096596', 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcarts`
--

CREATE TABLE `shoppingcarts` (
  `cartid` bigint(20) UNSIGNED NOT NULL,
  `productname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tprice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` blob DEFAULT NULL,
  `productid` bigint(20) UNSIGNED NOT NULL,
  `channelid` bigint(20) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shoppingcarts`
--

INSERT INTO `shoppingcarts` (`cartid`, `productname`, `quantity`, `price`, `tprice`, `image`, `productid`, `channelid`, `created_at`, `updated_at`) VALUES
(1, 'Iphone', '1', '1', '1', '', 1, 1, '2022-07-02 16:02:46', '2022-07-02 16:02:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_username_unique` (`username`);

--
-- Indexes for table `buyers`
--
ALTER TABLE `buyers`
  ADD PRIMARY KEY (`buyerid`),
  ADD UNIQUE KEY `buyers_username_unique` (`username`),
  ADD KEY `buyers_adminid_foreign` (`adminid`);

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`channelid`),
  ADD KEY `channels_adminid_foreign` (`adminid`),
  ADD KEY `channels_sellerid_foreign` (`sellerid`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `coupons_code_unique` (`code`);

--
-- Indexes for table `deliveris`
--
ALTER TABLE `deliveris`
  ADD PRIMARY KEY (`deliveriid`),
  ADD UNIQUE KEY `deliveris_username_unique` (`username`),
  ADD KEY `deliveris_adminid_foreign` (`adminid`),
  ADD KEY `deliveris_productid_foreign` (`productid`),
  ADD KEY `deliveris_orderid_foreign` (`orderid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `odetails`
--
ALTER TABLE `odetails`
  ADD PRIMARY KEY (`ordersdetailsid`),
  ADD KEY `odetails_productid_foreign` (`productid`),
  ADD KEY `odetails_orderid_foreign` (`orderid`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderid`),
  ADD KEY `orders_buyerid_foreign` (`buyerid`),
  ADD KEY `orders_productid_foreign` (`productid`),
  ADD KEY `orders_channelid_foreign` (`channelid`),
  ADD KEY `orders_paymentid_foreign` (`paymentid`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payments_bankaccountnumber_unique` (`bankaccountnumber`),
  ADD KEY `payments_adminid_foreign` (`adminid`),
  ADD KEY `payments_channelid_foreign` (`channelid`);

--
-- Indexes for table `pdetails`
--
ALTER TABLE `pdetails`
  ADD PRIMARY KEY (`pdetailid`),
  ADD KEY `pdetails_productid_foreign` (`productid`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productid`),
  ADD KEY `products_adminid_foreign` (`adminid`),
  ADD KEY `products_channelid_foreign` (`channelid`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`sellerid`),
  ADD UNIQUE KEY `sellers_username_unique` (`username`),
  ADD KEY `sellers_adminid_foreign` (`adminid`);

--
-- Indexes for table `shoppingcarts`
--
ALTER TABLE `shoppingcarts`
  ADD PRIMARY KEY (`cartid`),
  ADD KEY `shoppingcarts_productid_foreign` (`productid`),
  ADD KEY `shoppingcarts_channelid_foreign` (`channelid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buyers`
--
ALTER TABLE `buyers`
  MODIFY `buyerid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `channelid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `deliveris`
--
ALTER TABLE `deliveris`
  MODIFY `deliveriid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=201;

--
-- AUTO_INCREMENT for table `odetails`
--
ALTER TABLE `odetails`
  MODIFY `ordersdetailsid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pdetails`
--
ALTER TABLE `pdetails`
  MODIFY `pdetailid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `sellerid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `shoppingcarts`
--
ALTER TABLE `shoppingcarts`
  MODIFY `cartid` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `buyers`
--
ALTER TABLE `buyers`
  ADD CONSTRAINT `buyers_adminid_foreign` FOREIGN KEY (`adminid`) REFERENCES `admins` (`id`);

--
-- Constraints for table `channels`
--
ALTER TABLE `channels`
  ADD CONSTRAINT `channels_adminid_foreign` FOREIGN KEY (`adminid`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `channels_sellerid_foreign` FOREIGN KEY (`sellerid`) REFERENCES `sellers` (`sellerid`);

--
-- Constraints for table `deliveris`
--
ALTER TABLE `deliveris`
  ADD CONSTRAINT `deliveris_adminid_foreign` FOREIGN KEY (`adminid`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `deliveris_orderid_foreign` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`),
  ADD CONSTRAINT `deliveris_productid_foreign` FOREIGN KEY (`productid`) REFERENCES `products` (`productid`);

--
-- Constraints for table `odetails`
--
ALTER TABLE `odetails`
  ADD CONSTRAINT `odetails_orderid_foreign` FOREIGN KEY (`orderid`) REFERENCES `orders` (`orderid`),
  ADD CONSTRAINT `odetails_productid_foreign` FOREIGN KEY (`productid`) REFERENCES `products` (`productid`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_buyerid_foreign` FOREIGN KEY (`buyerid`) REFERENCES `buyers` (`buyerid`),
  ADD CONSTRAINT `orders_channelid_foreign` FOREIGN KEY (`channelid`) REFERENCES `channels` (`channelid`),
  ADD CONSTRAINT `orders_paymentid_foreign` FOREIGN KEY (`paymentid`) REFERENCES `payments` (`id`),
  ADD CONSTRAINT `orders_productid_foreign` FOREIGN KEY (`productid`) REFERENCES `products` (`productid`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_adminid_foreign` FOREIGN KEY (`adminid`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `payments_channelid_foreign` FOREIGN KEY (`channelid`) REFERENCES `channels` (`channelid`);

--
-- Constraints for table `pdetails`
--
ALTER TABLE `pdetails`
  ADD CONSTRAINT `pdetails_productid_foreign` FOREIGN KEY (`productid`) REFERENCES `products` (`productid`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_adminid_foreign` FOREIGN KEY (`adminid`) REFERENCES `admins` (`id`),
  ADD CONSTRAINT `products_channelid_foreign` FOREIGN KEY (`channelid`) REFERENCES `channels` (`channelid`);

--
-- Constraints for table `sellers`
--
ALTER TABLE `sellers`
  ADD CONSTRAINT `sellers_adminid_foreign` FOREIGN KEY (`adminid`) REFERENCES `admins` (`id`);

--
-- Constraints for table `shoppingcarts`
--
ALTER TABLE `shoppingcarts`
  ADD CONSTRAINT `shoppingcarts_channelid_foreign` FOREIGN KEY (`channelid`) REFERENCES `channels` (`channelid`),
  ADD CONSTRAINT `shoppingcarts_productid_foreign` FOREIGN KEY (`productid`) REFERENCES `products` (`productid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
