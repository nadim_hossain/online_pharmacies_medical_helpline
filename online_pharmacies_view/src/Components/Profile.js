import React, { Component } from "react";
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import axios from 'axios';
import swal from 'sweetalert';

const Profile = () => {
    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [studentInput, setStudent] = useState([]);
    const [errorInput, setError] = useState([]);

    useEffect(() =>{

        axios.get('/sanctum/csrf-cookie').then(response => {
        axios.get(`/api/profile`).then( res => {

            if(!localStorage.getItem('auth_token')){
                navigate('/login')
            }
            else{
                
                if(res.data.status === 200)
                {
                setStudent(res.data.admin);
                setLoading(false);
            }
            else
            {
                swal("Error",res.data.message,"error");
                navigate('/dashboard');
            }
            }
        });
    });

    }, []);

    const handleInput = (e) => {
        e.persist();
        setStudent({...studentInput, [e.target.name]: e.target.value });
    }


    if(loading)
    {
        return <h4>Loading Edit Student Data...</h4>
    }
    
    return (
        <div>
            <h1>MY PROFILE</h1>
            <hr />
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <h4>Profile</h4>
                            </div>
                            <div className="card-body">
                                
                                <form>
                                    <div className="form-group mb-3">
                                        <label>User Name</label>
                                        <input type="text" name="username" onChange={handleInput} value={studentInput.username}  className="form-control" readOnly />
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Full Name</label>
                                        <input type="text" name="name" onChange={handleInput} value={studentInput.name}  className="form-control" readOnly/>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Email Address</label>
                                        <input type="email" name="email" onChange={handleInput} value={studentInput.email}  className="form-control" readOnly />
                                    </div>
                                    <div className="form-group mb-3">
                                    <label>Gender: </label>
                                    <input type="text" name="gender" onChange={handleInput} value={studentInput.gender}  className="form-control" readOnly />
                                    </div>
                                    <div className="form-group mb-3">
                                    <label>Phone Number: </label>
                                    <input type="number" name="number" onChange={handleInput} value={studentInput.number}  className="form-control" readOnly />
                                    </div>
                                </form>
                                <br/>
                                    <div className="form-group mb-3">
                                        <Link to="/edit/admin/">
                                            <button className="btn btn-primary">Update User</button>
                                        </Link>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Profile;  