import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { Link } from "react-router-dom";
import { useNavigate } from 'react-router-dom';
import swal from "sweetalert";
import React from "react";
import axios from "axios";
import '../App.css'
const Header = () => {
  const navigate = useNavigate();
  const logoutSubmit = (e) => {
    e.preventDefault();

    axios.get('/sanctum/csrf-cookie').then(response => {
      axios.post(`api/logout/submit`).then(res =>{
          if(res.data.status === 200){
              localStorage.removeItem('auth_token');
              localStorage.removeItem('auth_name');
              swal("Success", res.data.message, "success");
              navigate('/login');
          }
      });
  });
  }

  var AuthButtons = '';
  var AuthButtons2 = '';
  if(!localStorage.getItem('auth_token')){
    AuthButtons2 = (
      <Link to="/login">Login</Link>
    )
  }
  else{
    AuthButtons = (
      <>      <Nav className="me-auto nav_bar_wrapper">
        <Link  to="/dashboard">Dashboard</Link>
              <Link to="/register">Users</Link>
              <Link to="/payments">Payments Getaway</Link>
              <Link to="/inventory">Inventory</Link>
              <Link to="/discount">Discount Coupons</Link>
              <Link to="/accounts">Accounts</Link>
              <Link to="/payment/clint">Payments</Link>
      </Nav>
      <Nav className="me-auto nav_bar_wrapper">
      <Link to="/profile">Profile</Link>
      <Link to="/logout" onClick={logoutSubmit}>Sign Out</Link>
      </Nav>
      </>

    )
  }

  return (
    <div className='App'>
      <div>
        <h1>Welcome Back, Admin</h1>
      </div>
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
          <Navbar.Brand href="/dashboard">Admin Panal</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto nav_bar_wrapper">
              {AuthButtons}
            </Nav>
            <Nav className="me-auto nav_bar_wrapper">
              {AuthButtons2}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      {/* <Nav variant="tabs" defaultActiveKey="/home">
      <Nav.Item>
        <Link to="/home">Active</Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="link-1">Option 2</Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link eventKey="disabled" disabled>
          Disabled
        </Nav.Link>
      </Nav.Item>
    </Nav> */}
            {/* <Link to="/">Home</Link>
            <Link to="/profile">Profile</Link>
            <Link to="/contact">Contact</Link>
            <Link to="/color">Color State</Link>
            <Link to="/effect">Effect State</Link>
            <Link to="/posts">All Posts</Link>
            <Link to="/login">Login</Link>
            <Link to="/allproducts">Products</Link>
            <Link to="/signout">SignOut</Link> */}
    </div>
  );
}

export default Header;