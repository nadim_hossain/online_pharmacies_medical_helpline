import React, { Component } from "react";
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
const Inventory = () => {
    const navigate = useNavigate();

    useEffect(() =>{
        if(!localStorage.getItem('auth_token')){
            navigate('/login')
        }
    },[])
    return (
        <h1>Inventory</h1>
    );
}

export default Inventory;  