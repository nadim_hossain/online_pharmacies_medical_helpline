import React, {useState, useEffect} from 'react';
import {Link, useHistory} from 'react-router-dom';
import axios from 'axios';
import swal from 'sweetalert';
import { useNavigate } from 'react-router-dom';
import { useParams } from "react-router-dom";
import ViewModal from './Modals/ViewModal';

function Users() {


    const navigate = useNavigate();
    const [loading, setLoading] = useState(true);
    const [students, setStudents] = useState([]);


    useEffect(() => {

        axios.get(`/api/customers`).then(res=>{
            if(res.status === 200)
            {
                setStudents(res.data.customers);
                setLoading(false);
            }
            else if(!localStorage.getItem('auth_token')){
                navigate('/login');
            }
        });

    }, []);

    const deleteStudent = (e, id) => {
        e.preventDefault();
        
        const thisClicked = e.currentTarget;
        thisClicked.innerText = "Deleting..";

        
            try {
                axios.get('/sanctum/csrf-cookie').then(response => {
                axios.delete(`api/delete/student/${id}`).then(res=>{
                    if(res.data.status === 200)
                    {
                        swal("Deleted!",res.data.message,"success");
                        thisClicked.closest("tr").remove();
                    }
                    else if(res.data.status === 404)
                    {
                        swal("Error",res.data.message,"error");
                        thisClicked.innerText = "Delete";
                    }
                });
             });
            } catch (error) {
                if (error.response){

                    console.log(error.response);
                    
                    }else if(error.request){
                    
                        console.log(error.request);
                    
                    }else if(error.message){
                    
                        console.log(error.message);
                    
                    }
            }
     
    }

    var student_HTMLTABLE = "";

    if(loading)
    {
        student_HTMLTABLE = <tr><td colSpan="7"> <h2>Loding.....</h2></td></tr>
    }
    else
    {
       
        student_HTMLTABLE = students.map( (item, index) => {
            return (
                <tr key={index}>
                    <td>{item.id}</td>
                    <td>{item.username}</td>
                    <td>{item.name}</td>
                    <td>{item.address}</td>
                    <td>{item.gender}</td>
                    <td>{item.mobile}</td>
                    <td>
                        <Link to={"/edit/student/"+item.id} className="btn btn-success btn-sm">Edit</Link>
                    </td>
                    <td>
                        <button type="button" onClick={(e) => deleteStudent(e, item.id)} className="btn btn-danger btn-sm">Delete</button>
                    </td>
                </tr>
            );
        });
    }

    return (
        <div>
            <h1>USERS PAGE</h1>
            <hr></hr>
            <br></br>
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                                <h4>Customers Data
                                    <Link to={'/adduser'} className="btn btn-primary btn-sm float-end"> Add Users</Link>
                                </h4>
                            </div>
                            <div className="card-body">
                                
                                <table className="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Username</th>
                                            <th>Full Name</th>
                                            <th>Address</th>
                                            <th>Gender</th>
                                            <th>Phone Number</th>
                                            {/* <th>Action</th> */}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {student_HTMLTABLE}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}

export default Users;