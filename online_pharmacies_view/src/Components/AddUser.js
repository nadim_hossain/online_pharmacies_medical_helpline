import axios, { Axios } from 'axios';
import swal from 'sweetalert';
import { useNavigate } from 'react-router-dom';
import { useState, useEffect } from 'react';
import '../App.css'
function AddUser() {
    const navigate = useNavigate();

    useEffect(() =>{
        if(!localStorage.getItem('auth_token')){
            navigate('/login')
        }
    },[])

    const [registerInput, setRegister] = useState({
        uname: '',
        fname: '',
        address: '',
        gender: '',
        password: '',
        cpassword: '',
        phone: '',
        error_list : [],
    });

    const heandleInput = (e) =>{
        e.persist();
        setRegister({...registerInput, [e.target.name]: e.target.value})
    }
    // const [username, setUname] = useState("");
    // const [name, setFname] = useState("");
    // const [address, setAddress] = useState("");
    // const [gender, setGender] = useState("");
    // const [mobile, setPhone] = useState("");
    // const [password, setPassword] = useState("");
    // const [cpassword, setCPassword] = useState("");

//     async function signup(){

//     let Item ={username,password,name,address,gender,mobile}

//     let result = await fetch("http://127.0.0.1:8000/api/register",{
//         method:'POST',
//         headers:{
//             "Content-Type":"application/json",
//             "Accept":"application/json"
//         },
//         body:JSON.stringify(Item)
//     });
//     result = await result.json();
//     console.warn("result", result)
//     console.warn(Item);
// }

const registerSubmit = (e) =>{

    e.preventDefault();

    const data = {
        uname: registerInput.uname,
        fname: registerInput.fname,
        address: registerInput.address,
        gender: registerInput.gender,
        password: registerInput.password,
        cpassword: registerInput.cpassword,
        phone: registerInput.phone,
    }
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post(`/api/register`, data).then(res => {
                if(res.data.status === 200){
                    localStorage.setItem('auth_token', res.data.token);
                    localStorage.setItem('auth_name', res.data.username);
                    swal("Success", res.data.message, "success");
                    navigate('/register');
                }
                else
                setRegister({...registerInput, error_list: res.data.errors})
            });
        });
}

  return (
    <div className="col-sm-6 offset-sm-3">
        <h1>Add Users</h1>
        <hr></hr>
                        <br/>
                        <form onSubmit={registerSubmit}>
                            <label>Username :</label>
                            <input type="text" placeholder="User Name" name='uname' value={registerInput.uname} onChange={heandleInput} className="form-control" />
                            <span className="text-danger">{registerInput.error_list.uname}</span>
                            <br/>
                        <label>Full Name :</label> 
                        <input type="text" placeholder="First Name" name='fname' value={registerInput.fname} onChange={heandleInput} className="form-control"/>
                        <span className="text-danger">{registerInput.error_list.fname}</span>
                        <br/>
                        <label>Address :</label> 
                        <input type="text" placeholder="Enter Address" name='address' value={registerInput.address} onChange={heandleInput} className="form-control"/>
                        <span className="text-danger">{registerInput.error_list.address}</span>
                        <br/>
                        <label>Gender: </label>
                        <select className="form-control" name='gender' value={registerInput.gender} onChange={heandleInput} >

                        <option>Select One</option>
                        <option>Male</option>
                        <option>Female</option>
                        <option>Other</option>

                        </select>
                        <span className="text-danger">{registerInput.error_list.gender}</span>
                            <br/>
                        <label>Password :</label> 
                        <input type="password"  name='password' placeholder="Enter Password" value={registerInput.password} onChange={heandleInput} className="form-control"/>
                        <span className="text-danger">{registerInput.error_list.password}</span>
                                <br/>
                        <label>Confirm Password :</label>
                        <input type="password" name='cpassword' value={registerInput.cpassword} onChange={heandleInput} placeholder="Enter Password" className="form-control"/>
                        <span className="text-danger">{registerInput.error_list.cpassword}</span>
                                <br/>
                        <label>Phone Number :</label>
                        <input type="number" name='phone' value={registerInput.phone} onChange={heandleInput} placeholder="Enter Phone Number" className="form-control"/>
                        <span className="text-danger">{registerInput.error_list.phone}</span>
                            <br/>
                            {/* <label>Upload Image :</label>
                            <input type="file" className="form-control"/>
                            <br/> */}
                    <button className="btn btn-primary  btn-sm float-end">Submit</button>
                    <br></br>
                </form>
    </div>
  );
}

export default AddUser;