import axios from "axios";
import React, { Component } from "react";
import { useState, useEffect } from 'react';
import swal from "sweetalert";
import { useNavigate } from 'react-router-dom';
const Change = () => {
    const navigate = useNavigate();

    useEffect(() =>{
        if(localStorage.getItem('code') === null){

            navigate('/verify/user');
        }
    },[])
    // const [username, setUname] = useState("");
    // const [password, setPassword] = useState("");


    // async function loginsubmit(){
    //     let Item ={username,password}
    //     console.warn(Item);
    // }
    const [loginInput, setLogin] = useState({
        password: '',
        confirmpassword: '',
        error_list: [],
    });

    const heandleInput = (e)=>{
        e.persist();
        setLogin({...loginInput, [e.target.name]: e.target.value});
    }

    const loginSubmit = (e) =>{
        e.preventDefault();
        const data = {
            password: loginInput.password,
            confirmpassword: loginInput.confirmpassword,

        }
        axios.get('/sanctum/csrf-cookie').then(response => {
            axios.post(`api/change`, data).then(res =>{
                if(res.data.status === 200){
                    swal("Success", res.data.message, "success");
                    navigate('/login');
                }
                else if(res.data.status === 401){
                    swal("Warning",res.data.message,"warning");
                }
                else
                setLogin({...loginInput, error_list: res.data.errors});
            });
        });
    }
    
    return (
        <div className="col-sm-6 offset-sm-3">
                <h1>CHANGE PASSWORD  PAGE</h1>
                <hr></hr>
                        <br/>
                <form onSubmit={loginSubmit}>
                            <label>Password :</label>
                            <input type="password" placeholder="Enter Password" name="password" value={loginInput.password} onChange={heandleInput} className="form-control" />
                            <span>{loginInput.error_list.password}</span>
                            <br/>
                            <label>Confirm Password :</label>
                            <input type="password" placeholder="Enter Confirm Password" name="confirmpassword" value={loginInput.confirmpassword} onChange={heandleInput} className="form-control" />
                            <span>{loginInput.error_list.confirmpassword}</span>
                            <br/>
                    <button className="btn btn-primary" >Submit</button>
                    <br></br>
                </form>
                <br></br>
        </div>
    );
}

export default Change;