import React, { Component } from "react";
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
const Discount_Coupons = () => {

    const navigate = useNavigate();

    useEffect(() =>{
        if(!localStorage.getItem('auth_token')){
            navigate('/login')
        }
    },[])
    return (
        <h1>Discount Coupons</h1>
    );
}

export default Discount_Coupons;  