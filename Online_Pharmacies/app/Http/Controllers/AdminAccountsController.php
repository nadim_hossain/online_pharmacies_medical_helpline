<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AdminAccountsController extends Controller
{
    public function __construct(){
        $this->middleware('ValidAdmin');
    }

    public function accounts(){

        $earm = Order::where('status', 'Done')->sum('tprice');

        $tex = $earm * (15)/100;

        $liabilities = $earm - ($tex + DB::table("payments")->sum('amount'));
        $equaty =$tex;

        $mytime = Carbon::today();

        $tmoney = Order::where('status', 'Done')
        ->where('date', $mytime)
        ->sum('tprice');

        $moneys = Order::where('status', 'Done')->get();
        $moneys = Order::paginate(3);

        $pays = Payment::all();
        $pays = Payment::paginate(3);

        return view('admin.AdminAccounts', compact('earm', 'liabilities', 'equaty', 'tmoney', 'moneys', 'pays'));
    }


}
