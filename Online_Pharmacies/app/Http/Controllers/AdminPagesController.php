<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminPagesController extends Controller
{

    public function verify(){
        return view('admin.AdminVerify');
    }

}