<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Payment;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct(){
        $this->middleware('ValidAdmin');
    }

    public function index()
    {
        $data = DB::table("orders")
                    ->join('channels as ch','ch.channelid', '=', 'orders.channelid')
                    ->where('orders.status', '=', 'Done')
                    ->select(DB::raw("SUM(tprice) as sell_amount,orders.channelid, ch.channelname, ch.bankaccountnumber"))
                    ->orderBy("orders.channelid")->groupBy(DB::raw("orders.channelid", "ch.channelname", "ch.bankaccountnumber"))->get();
        foreach ($data as $key => $value) {
            $value->equityAmount= $value->sell_amount*0.15;
            $value->dueAmount= $value->sell_amount*0.85;
            $paymentAmount = DB::table("payments")
            ->where('channelid', '=', $value->channelid)->sum('amount');
            $value->dueAmount= $value->dueAmount-$paymentAmount;
        }
                    return view('admin.AdminPayment', compact('data'));
    }

    public function paymentsubmit (Request $request){

        $validate = $request->validate([
            "channel"=>"required|not_in:0",
            "amount"=>"required",
            "bank"=>"required|max:11|min:11"
            ],
            
        );

        if($validate){

            $check = Payment::where('channelname', '=', $request->channel)
                            ->where('bankaccountnumber', '=', $request->bank)->first();


            if($check){

                $data = DB::table("orders")
                    ->join('channels as ch','ch.channelid', '=', 'orders.channelid')
                    ->where('orders.status', '=', 'Done')
                    ->select(DB::raw("SUM(tprice) as sell_amount,orders.channelid, ch.channelname, ch.bankaccountnumber"))
                    ->orderBy("orders.channelid")->groupBy(DB::raw("orders.channelid", "ch.channelname", "ch.bankaccountnumber"))->get();

                foreach ($data as $key => $value) {
                    $value->equityAmount= $value->sell_amount*0.15;
                    $value->dueAmount= $value->sell_amount*0.85;
                    $paymentAmount = DB::table("payments")
                    ->where('channelid', '=', $value->channelid)->sum('amount');
                    $value->dueAmount= $value->dueAmount-$paymentAmount;
                }

                foreach ($data as $value) {
                    $due = $value->dueAmount;
                }

                if($due < $request->amount){
                    return Redirect()->route('payment.channel')
                        ->with('error2', 'Insufficient balance');
                }
                else{

                    $pay = Payment::where('channelname', '=', $request->channel)->first();

                    $pay->amount += $request->amount;
                    $pay->save();

                    return Redirect()->route('payment.channel')
                    ->with('success', 'Payment Successfull');
                }
            }
            else{
                return Redirect()->route('payment.channel')
                ->with('error', 'Something is wrong try again');
            }

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
