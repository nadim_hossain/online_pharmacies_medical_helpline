<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminChangePasswordController extends Controller
{

    public function __construct(){
        $this->middleware('ValidAdmin');
    }

    public function change_password(){
        return view('admin.AdminChangePassword');
    }
    
    public function changepassword(Request $request ){
        $validate = $request->validate( [
            "oldpassword"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            "newpassword"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            "retypenew"=>'required|min:6|same:newpassword|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/'
            ],

            
        );

        $name = session('admin');

        if($validate){

            $check = Admin::where('username', $name)->first();

            $pass = $check->password;

            if($pass === $request->oldpassword){
               
                $admins = Admin::where('username', $name)->first();

                    $admins->password = $request->retypenew;

                    $admins->save();

                return Redirect()->route('change_password')
                ->with('success','Updated Successfull');
            }
            else{
                $error = "Something is wrong. try again.";
                return Redirect()->route('change_password')
                ->with('error', $error);
            }
        }
    }
}
