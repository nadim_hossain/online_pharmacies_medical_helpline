<?php

namespace App\Http\Controllers;

use App\Exports\BuyerExport;
use App\Exports\SellerExport;
use App\Exports\PaymentDueExport;
use App\Exports\PaymentDueExportView;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class BuyerExportController extends Controller
{
    public function buyerexport(){

        return Excel::download(new BuyerExport, 'Buyers.xlsx');
    }

    public function sellerexport(){

        return Excel::download(new SellerExport, 'Sellers.xlsx');
    }
}
