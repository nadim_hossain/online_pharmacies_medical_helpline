<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Order;
use App\Models\Seller;
use Carbon\Carbon;

class AdminInventoryController extends Controller
{
    public function __construct(){
        $this->middleware('ValidAdmin');
    }

    public function inventory(){

        $mytime = Carbon::today();


        $products = Product::sum('quantity');

        $tproducts = Product::where('date', $mytime)->sum('quantity');

        $tmoney = Order::where('status', 'Done')
        ->where('date', $mytime)
        ->sum('tprice');

        $tsales = Order::where('status', 'Done')
        ->where('date', $mytime)
        ->sum('quantity');

        $pname = Seller::join('channels', 'channels.sellerid', '=', 'sellers.sellerid')
                            ->join('products', 'products.channelid', '=', 'channels.channelid')
                            ->get(['channels.channelname', 'products.productname', 'products.productid', 'products.date', 'products.quantity']);

        return view('admin.AdminInventory', compact('products', 'tproducts', 'tsales', 'tmoney', 'pname'));
    }
}
