<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminAddSellersController extends Controller
{
    public function __construct(){
        $this->middleware('ValidAdmin');
    }   

    public function addseller(){
        return view('admin.AdminAddSellers');
    }

    public function addsellersubmit(Request $request){

        $validate = $request->validate([
            "uname"=>"required|min:5|max:10",
            "fname"=>"required|min:5|max:20",
            "address"=>"required|min:5|max:20",
            "password"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            "cpassword"=>'required|min:6|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x])(?=.*[!$#%]).*$/',
            'Phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/'
            ],
            
        );

        if($validate){

           $uname = $request->uname;
           $fname = $request->fname;
           $address = $request->address;
           $cpassword = $request->cpassword;
           $phone = $request->Phone;
           $image = $request->image;


           DB::table('sellers')->insert(
                array(

                    'username' => $uname,
                    'password' => $cpassword,
                    'name' => $fname,
                    'address' => $address,
                    'status' => 'active',
                    'image' => $image,
                    'number' => $phone,
                )

           );

            $error = "Registraion Successfull";
            return Redirect()->route('customers')
            ->with('success', $error);
        }
    }
}
