<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CouponController extends Controller
{

    public function __construct(){
        $this->middleware('ValidAdmin');
    }

    public function discount(){
        $mytime = Carbon::today();

        $coupon = Coupon::all();

        $coupon = Coupon::where('date' ,'<', $mytime);

        if($coupon){
            $coupon = "Expired";
        }

        else{
            $coupon = "Valid";
        }

        $coupon = Coupon::paginate(5);

        return view('admin.AdminDiscount', compact('coupon'));
    }

    public function couponsubmit (Request $request){

        $validate = $request->validate([
            "code"=>"required",
            "closeddate"=>"required",
            "value"=>"required"
            ],
            
        );

        if($validate){

            $code = $request->code;
           $date = $request->closeddate;
           $value = $request->value;


           DB::table('coupons')->insert(
                array(

                    'code' => $code,
                    'date' => $date,
                    'value' => $value
                )

           );

        }

            $succ = "Coupon has been Genarated";
            return Redirect()->route('discount')
            ->with('success', $succ);
    }

    function action3(Request $request)
    {
    	if($request->ajax())
    	{
    		if($request->action == 'edit')
    		{
    			$data = array(
    				'id' 	        =>	$request->id,
    				'code'		    =>	$request->code,
					'date'	   		=>	$request->closeddate,
    				'value'		    =>	$request->value
    			);
    			DB::table('coupons')
    				->where('id', $request->id)
    				->update($data);
    		}
    		if($request->action == 'delete')
    		{
    			DB::table('coupons')
    				->where('id', $request->id)
    				->delete();
    		}
    		return response()->json($request);
    	}
    }
}
