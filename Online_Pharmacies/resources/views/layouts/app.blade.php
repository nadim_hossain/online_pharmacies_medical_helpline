<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Navigation</title>
    

<style>
    @import url("https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap");
  :root {
    --header-height: 3rem;
    --nav-width: 215px;
    --first-color: #222222;
    --first-color-light: #afa5d9;
    --white-color: #f7f6fb;
    --body-font: "Nunito", sans-serif;
    --normal-font-size: 1rem;
    --z-fixed: 100;
  }
  *,
  ::before,
  ::after {
    box-sizing: border-box;
  }
  body {
    position: relative;
    margin: calc(var(--header-height) + 2rem) 0 0 0;
    padding-left: calc(var(--nav-width) + 2rem);
    font-family: var(--body-font);
    font-size: var(--normal-font-size);
    transition: 0.5s;
  }
  a {
    text-decoration: none;
  }
  .header {
    width: 100%;
    height: calc(var(--header-height) + 1rem);
    padding: 0 2rem 0 calc(var(--nav-width) + 55rem);
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: space-between;
    background-color: var(--white-color);
    z-index: var(--z-fixed);
    transition: 0.5s;
  }
  .header_toggle {
    color: var(--first-color);
    font-size: 1.5rem;
    cursor: default;
  }
  .header_img {
    width: 35px;
    height: 35px;
    display: flex;
    justify-content: center;
    border-radius: 50%;
    overflow: hidden;
  }
  .header_img img {
    width: 40px;
  }
  .l-navbar {
    position: fixed;
    top: 0;
    left: -30%;
    width: var(--nav-width);
    height: 100vh;
    background-color: var(--first-color);
    padding: 0.5rem 1rem 0 0;
    transition: 0.5s;
    z-index: var(--z-fixed);
  }
  .nav {
    height: 100%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    overflow: hidden;
  }
  .nav_logo,
  .nav_link {
    display: grid;
    grid-template-columns: max-content max-content;
    align-items: center;
    column-gap: 1rem;
    padding: 0.5rem 0 0.5rem 1.5rem;
  }
  .nav_logo {
    margin-bottom: 2rem;
  }
  .nav_logo-icon {
    font-size: 1.25rem;
    color: var(--white-color);
  }
  .nav_logo-name {
    color: var(--white-color);
    font-weight: 700;
  }
  .nav_link {
    position: relative;
    color: var(--first-color-light);
    margin-bottom: 1.5rem;
    transition: 0.3s;
  }
  .nav_link:hover {
    color: var(--white-color);
  }
  .nav_icon {
    font-size: 1.25rem;
  }
  .show {
    left: 0;
  }
  .body-pd {
    padding-left: calc(var(--nav-width) + 1rem);
  }
  .active {
    color: var(--white-color);
  }
  .active::before {
    content: "";
    position: absolute;
    left: 0;
    width: 2px;
    height: 32px;
    background-color: var(--white-color);
  }
  .height-100 {
    height: 100vh;
  }
  @media screen and (min-width: 768px) {
    body {
      margin: calc(var(--header-height) + 2rem) 0 0 0;
    padding-left: calc(var(--nav-width) + 2rem);
    }
    .header {
      height: calc(var(--header-height) + 1rem);
    }
    .header_img {
      width: 40px;
      height: 40px;
    }
    .header_img img {
      width: 45px;
    }
    .l-navbar {
      left: 0;
      padding: 1rem 1rem 0 0;
    }
    .show {
      width: calc(var(--nav-width) + 156px);
    }
    .body-pd {
      padding-left: calc(var(--nav-width) + 188px);
    }
    p {
    margin: 0px 0px 0px 10px;
    font-size: large;
    }

    .nav .open>a, .nav .open>a:focus, .nav .open>a:hover {
    background-color: #222222;
    border-color: #222222;
}
img {
    vertical-align: middle;
    margin-top: -88px;
    margin-bottom: -80px;
}

.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 15px;
    margin-top: -35px;
}
.dropdown-menu>li>a {
    display: block;
    padding: 3px 20px;
    clear: both;
    font-weight: 400;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    color: snow;
}
.open>.dropdown-menu {
    display: block;
    background-color: #222222;
    /* color: white; */
}
  }
</style>

</head>
<body>
  <body id="body-pd">
    <header class="header" id="header" style="text-align: center;">
      <div class="header_toggle" style="text-align: center;"> 
        <i class='bx bx-menu' id="header-toggle"></i> 
        <b><center style="text-align: center;"><p style="align-content: center;">WELCOME BACK {{session('admin')}}</p></center></b>
      </div>
        <div class="header_img"> 
        <a class="nav_link" href="{{route('dashboard')}}">  <img src="https://i.imgur.com/hczKIze.jpg" alt=""> </a>
        </div> 
    </header>
    <div class="l-navbar" id="nav-bar">
      <nav class="nav">
        <img src="{{asset('images/logo.png')}}" alt="">
        <div> 
          <div class="nav_list"> 
            <a class="nav_link" href="{{route('dashboard')}}"> 
              <i class='bx bx-grid-alt nav_icon'></i> 
              <span class="nav_name"> <span class="glyphicon glyphicon-home"></span>  Dashboard</span> 
            </a> 
            <a class="nav_link" href="{{route('customers')}}"> 
              <i class='bx bx-user nav_icon'>
              </i> <span class="nav_name"><span class="	glyphicon glyphicon-user">  </span> Users</span> 
            </a> <a href="{{route('payments')}}" class="nav_link"> 
              <i class='bx bx-message-square-detail nav_icon'></i> 
              <span class="nav_name"> <span class="	glyphicon glyphicon-usd"> </span> Payments Getaway</span> 
            </a> <a href="{{route('inventory')}}" class="nav_link"> 
              <i class='bx bx-bookmark nav_icon'></i>
              <span class="nav_name"> <span class="	glyphicon glyphicon-shopping-cart"></span> Inventory</span> 
            </a> <a href="{{route('discount')}}" class="nav_link"> 
              <i class='bx bx-folder nav_icon'></i> 
              <span class="nav_name"> <span class="glyphicon glyphicon-tags"></span> Discount Coupons</span> 
            </a> 
            <a href="{{route('accounts')}}" class="nav_link"> 
              <i class='bx bx-bar-chart-alt-2 nav_icon'>
              </i> <span class="nav_name"> <span class="glyphicon glyphicon-credit-card"></span> Accounts</span>
            </a>
            <a href="{{route('payment.channel')}}" class="nav_link"> 
              <i class='bx bx-bar-chart-alt-2 nav_icon'>
              </i> <span class="nav_name"> <span class="glyphicon glyphicon-usd"></span> Payment</span>
            </a>
            <a href="{{route('profile')}}" class="nav_link"> 
              <i class='bx bx-bar-chart-alt-2 nav_icon'>
              </i> <span class="nav_name"> <span class="	glyphicon glyphicon-user"> </span> Profile</span>
            </a>
          </div>
        </div>
        <li style="background-color: #222222;"><a href="" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Settings <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="{{route('logout')}}"> <span class="glyphicon glyphicon-off"></span> Sign Out</a></li>
                <li><a href="{{route('change_password')}}"> <span class="glyphicon glyphicon-lock"></span> Change Password</a></li>
              </ul>
            </li>
        </a>
      </nav>
    </div>
    <!--Container Main start-->
    <div class="height-100 bg-light">
      @yield('content')
    </div>
    <!--Container Main end-->
</body>
</html>