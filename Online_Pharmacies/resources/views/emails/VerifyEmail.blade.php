@component('mail::message')
# {{ $details['title'] }}



<p>Your One Time Password is {{$details['code']}}</p>



Thanks,<br>
{{ config('app.name') }}
@endcomponent