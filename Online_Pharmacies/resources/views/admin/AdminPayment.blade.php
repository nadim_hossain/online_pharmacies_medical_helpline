<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }
        input[type=text],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
input[type=submit]:hover {
    background-color: #45a049;
}
input[type=number],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=date],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
            <div>
                <h1 style="font-style: italic;"  >PAYMENT METHODE</h1>
            </div>
            <hr>
            <br>
                <div id="container">
            @if(session()->has('success'))
                <div class="alert alert-success">
                {{ session()->get('success') }}
                </div>
            @endif
            <div id="container">
            @if(session()->has('error'))
                <div class="alert alert-danger">
                {{ session()->get('error') }}
                </div>
            @endif
            <div id="container">
            @if(session()->has('error2'))
                <div class="alert alert-danger">
                {{ session()->get('error2') }}
                </div>
            @endif
                <form action="{{route('payment.channel')}}" method="POST" id="form">
                    {{csrf_field()}}
                <div id="container1">
                        <br>
                        <div class="col-75">
                        <label>Channels :</label>
                        <select class="form-control" id="channel" name="channel">
                                            <b><span class="formerror" id="error3"></span></b>
                                            <option value="0" >Select One</option>
                                            @foreach($data as $row)
                                                    <option>{{$row->channelname}}</option>
                                            @endforeach
                        </select>
                            @error('channel')
                                    <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>
                        <label>Amounts </label>
                        <input type="number" id="amount" name="amount" placeholder="Enter Any Amount" value="{{old('amound')}}" class="form-control" >
                            @error('amount')
                                    <span class="text-danger">{{$message}}</span>
                            @enderror
                        <div class="col-75">
                        <label>Bank Account Number :</label> <input type="number" id="bank" name="bank" placeholder="Enter Account Number" value="{{old('bank')}}" class="form-control">
                        @error('bank')
                                <span class="text-danger">{{$message}}</span>
                        @enderror
                        </div>
                        <input type="submit" name="Start" value="Payment">
            </div>
		</form>
        <br>
        <hr>
        <br>
        <div id="table1">
                <div id="UserTable">

                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title"> <span class="glyphicon glyphicon-usd">  </span> Payments Details</h3>
                    </div>
                    <div class="panel-body">
                <div class="table-responsive">
                        @csrf
                    <table id="editable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Channel Name</th>
                            <th>Assets</th>
                            <th>Due Amount</th>
                            <th>Equity Amount</th>
                            <th>Bank Account Number</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($data as $row)
                            <tr>
                            <td>{{ $row->channelid }}</td>
                            <td>{{ $row->channelname }}</td>
                            <td>{{ $row->sell_amount }}</td>
                            <td>{{ $row->dueAmount }}</td>
                            <td>{{ $row->equityAmount }}</td>
                            <td>{{ $row->bankaccountnumber }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    <nav aria-label="Page navigation example" style="margin-left: 550px;">
                        <ul class="pagination justify-content-end">
                        </ul>
                        </nav>
                    </div>
                </div>
                </div>
                </div>
    @endsection
</body>
</html>