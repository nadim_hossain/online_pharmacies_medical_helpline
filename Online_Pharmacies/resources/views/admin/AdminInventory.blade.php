<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inventory Management</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }

        div#box {
        margin-top: -4px;
        border-radius: 0px;
        background-color: white;
        padding: 53px;
        margin-left: -111px;
        margin-right: 0px;
        }
    
        div#pending {
            width: 304px;
            padding: 16px;
            margin-left: 54px;
            text-align: justify;
            border-radius: 8px;
            background-color: #337AB7;
            color: white;
            font-family: inherit;
            margin-bottom: 22px;
            margin-top: -38px;
        }
    
        div#accepted {
            width: 304px;
            padding: 16px;
            border-color: 3px solid green;
            margin-left: 381px;
            margin-top: -171px;
            border-radius: 8px;
            background-color: #5CB85C;
            color: white;
            font-family: inherit;
        }
    
        div#completed {
            width: 304px;
            padding: 16px;
            /* border: 3px solid green; */
            margin-left: 708px;
            margin-top: -149px;
            border-radius: 8px;
            background-color: #F0AD4E;
            color: white;
            font-family: inherit;
        }
    
        div#rejected {
            width: 304px;
            padding: 16px;
            /* border: 3px solid green; */
            margin-left: 1036px;
            margin-top: -147px;
            border-radius: 8px;
            background-color: #D9534F;
            color: white;
            font-family: inherit;
        }
        div#total {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #337AB7;
        }
        div#total2 {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #5CB85C;
        }
        div#total3 {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #F0AD4E;
        }
        div#total4 {
            margin: 62px 0px 0px 0px;
            text-align: right;
            background-color: #D9534F;
        }
        div#UserTable{
            margin-right: 600px;
        }
        div#UserTable2{
            margin-left: 720px;
            margin-top: -377px;
        }
    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
            <div>
                <h1 style="font-style: italic;">MY INVENTORY</h1>
            </div>
            <hr>
            <div id="box">
                <div id="boder">
                    <div id="pending">
                        <div id="total">
                        <div id="countusers">
                            <b><h3>
                            {{$products}}
                            </h3></b>
                            </div>
                            <b>Total Products</b><br>
                        </div>
                    </div>
                </div>
                <div id="accepted">
                    <div id="total2">
                    <div id="countusers">
                            <b><h3>
                            {{$tproducts}}
                            </h3></b>
                            </div>
                        <b>Total Added New Products </b><br>
                    <b></b>
                    </div>
                </div>
                <div id="completed">
                    <div id="total3">
                    <div id="countusers">
                            <b><h3>
                            {{$tsales}}
                            </h3></b>
                            </div>
                        <b>Today Products Selas</b><br>
                    <b></b>
                    </div>
                </div>
                <div id="rejected">
                    <div id="total4">
                    <div id="countusers">
                            <b><h3>
                            {{$tmoney}}
                            </h3></b>
                            </div>
                        <b>Total Today Selas TK</b><br>
                    <b></b>
                    </div>
                </div >
	        </div>
            <hr>
            <div id="UserTable">

                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title"> <span class="glyphicon glyphicon-shopping-cart">  </span> Products</h3>
                    </div>
                    <div class="panel-body">
                    <div class="table-responsive">
                        @csrf
                        <table id="editable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Products Name</th>
                            <th>Channel Name</th>
                            <th>Quantity</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($pname as $name)
                                    <tr>
                                        <td>{{ $name->productid }}</td>
                                        <td>{{ $name->productname }}</td>
                                        <td>{{ $name->channelname }}</td>
                                        <td>{{ $name->quantity }}</td>
                                    </tr>
                                @endforeach
                        </tbody>
                        </table>
                    </div>
                    <nav aria-label="Page navigation example" style="margin-left: 1070px;">
                        <ul class="pagination justify-content-end">
                        
                        </ul>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
            <div id="UserTable2">

                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title"> <span class="glyphicon glyphicon-bell">  </span> Recent Add Product</h3>
                    </div>
                    <div class="panel-body">
                    <div class="table-responsive">
                        @csrf
                        <table id="editable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Products Name</th>
                            <th>Products Qunatity</th>
                            <th>Channel Name</th>
                            <th>Date</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($pname as $name)


                        @php($diffInDays = \Carbon\Carbon::parse($name->date)->diffInDays())

                            @php($showDiff = \Carbon\Carbon::parse($name->date)->diffForHumans())

                            @if($diffInDays > 0)

                                @php($showDiff .= ', '.\Carbon\Carbon::parse($name->date)->addDays($diffInDays)->diffInHours().' Hours')

                            @endif
                                    <tr>
                                        <td>{{ $name->productid }}</td>
                                        <td>{{ $name->productname }}</td>
                                        <td>{{ $name->quantity }}</td>
                                        <td>{{ $name->channelname }}</td>
                                        <td>{{ $showDiff}}</td>
                                    </tr>
                                @endforeach
                        </tbody>
                        </table>
                    </div>
                    <nav aria-label="Page navigation example" style="margin-left: 1070px;">
                        <ul class="pagination justify-content-end">
                        
                        </ul>
                        </nav>
                    </div>
                </div>
                </div>
            </div>
    @endsection
</body>
</html>