<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Discounts</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }
        input[type=text],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}
input[type=submit]:hover {
    background-color: #45a049;
}
input[type=number],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

input[type=date],
select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
            <div>
                <h1 style="font-style: italic;"  >DISCOUNTS COUPONS</h1>
            </div>
            <hr>
            <div id="container">
            @if(session()->has('success'))
                <div class="alert alert-success">
                {{ session()->get('success') }}
                </div>
            @endif
                <form action="{{route('discount')}}" method="POST" id="form">
                    {{csrf_field()}}
                <div id="container1">
                        <br>
                        <div class="col-75">
                        <label>Code :</label>
                        <input type="text" id="code" name="code" placeholder="Enter Random Value" value="{{old('code')}}" class="form-control">
                        @error('code')
                                <span class="text-danger">{{$message}}</span>
                        @enderror
                        <br>
                        </div>
                        <label>Coupon Closed Date : </label>
                        <input type="date" id="cdate" name="closeddate" placeholder="Select Date" value="{{old('closeddate')}}" class="form-control" >
                            @error('closeddate')
                                    <span class="text-danger">{{$message}}</span>
                            @enderror
                        <br>
                        <div class="col-75">
                        <label>Value :</label> <input type="number" id="value" name="value" placeholder="Enter Numbers" value="{{old('value')}}" class="form-control">
                        @error('value')
                                <span class="text-danger">{{$message}}</span>
                        @enderror
                        </div>
                        <input type="submit" name="Start" value="Add">
            </div>
            <br>
		</form>
        <br>
        <hr>
        <div id="UserTable">

                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h3 class="panel-title"> <span class="glyphicon glyphicon-tags">  </span> Coupons Details</h3>
                    </div>
                    <div class="panel-body">
                <div class="table-responsive">
                        @csrf
                    <table id="editable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>ID</th>
                            <th>Code Name</th>
                            <th>Value</th>
                            <th>Created Date</th>
                            <th>Expired Date</th>
                            <th>Action</th>
                            </tr>
                        </thead>
                        <tbody id="myTable">
                        @foreach($coupon as $coupons)
                            <tr>
                            <td>{{ $coupons->id }}</td>
                            <td>{{ $coupons->code }}</td>
                            <td>{{ $coupons->value }}</td>
                            <td>{{ $coupons->created_at }}</td>
                            <td>{{ $coupons->date }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                    <nav aria-label="Page navigation example" style="margin-left: 550px;">
                        <ul class="pagination justify-content-end">
                        
                        </ul>
                        </nav>
                    </div>
                </div>
                </div>
                </div>
	</div>
    @endsection
</body>
</html>

<script>

$(document).ready(function(){
   
   $.ajaxSetup({
     headers:{
       'X-CSRF-Token' : $("input[name=_token]").val()
     }
   });
 
   $('#editable').Tabledit({
     url:'{{ route("coupon.action") }}',
     dataType:"json",
     columns:{
       identifier:[0, 'id'],
       editable:[[1, 'code'], [2, 'value'], [4, 'date']]
     },
     restoreButton:false,
     onSuccess:function(data, textStatus, jqXHR)
     {
       if(data.action == 'delete')
       {
         $('#'+data.id).remove();
       }
     }
   });
 
 });
</script>