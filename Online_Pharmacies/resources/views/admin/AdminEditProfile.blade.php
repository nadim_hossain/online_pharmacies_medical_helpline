<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Profile</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }
    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
        <div>
                <h1 style="font-style: italic;"  >EDIT PROFILE</h1>
            </div>
            <hr>
            <div id="profile">
                        <form action="{{route('adminEdit')}}" method="POST" id="form">
                    <div>
                         {{csrf_field()}}
                        <br>
                            <table class="table table-bordered table-hover">
                                    <tr align="center">
                                        <td colspan="6" class="active" style="background-color: steelblue;"><h2>My Profile</h2></td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" id="space">ID</td>
                                        <td id="space">
                                            <input class="form-control" id="adminid" type="text" name="adminid" value="{{$edits->id}}" readonly>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" id="space">User Name</td>
                                        <td id="space">
                                            <input class="form-control" id="username" type="text" name="username" value="{{$edits->username}}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" id="space">Full Name</td>
                                        <td id="space">
                                        <input class="form-control" id="fullname" type="text" name="fullname" value="{{$edits->name}}">
                                        
                                        </td>
                                    </tr>
                                        <tr>
                                        <td style="font-weight: bold;" id="space">Email Address</td></td>
                                        <td>
                                        <input class="form-control" id="email" type="text" name="email" value="{{$edits->email}}">
                                            
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" id="space">Gender</td>
                                        <td id="space">
                                        <select class="form-control" id="gender" name="gender">
                                            <b><span class="formerror" id="error3"></span></b>
                                                <option>{{$edits->gender}}</option>

                                                    <option>Male</option>
                                                    <option>Female</option>
                                                    <option>Other</option>

                                            </select>
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: bold;" id="space">Phone Number</td>
                                        <td id="space">
                                        <input class="form-control" id="number" type="text" name="number" value="{{$edits->number}}">
                                            
                                        </td>
                                    </tr>
                                    <tr align="center">
                                    <td colspan="6">
                                        <input type="submit" class="btn btn-info" name="update" style="width:250px;" value="Update">
                                    </td>
                                </tr>
                            </table>
                    </div>
                    </form>
                    <hr>
                    <br>
                </div>
            <hr>
    @endsection
</body>
</html>