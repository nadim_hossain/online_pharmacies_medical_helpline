<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My Profile</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css" integrity="sha384-eoTu3+HydHRBIjnCVwsFyCpUDZHZSFKEJD0mc3ZqSBSb6YhZzRHeiomAUWCstIWo" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
    <script src="https://markcell.github.io/jquery-tabledit/assets/js/tabledit.min.js"></script>

    <style>
        *, ::before, ::after {
            box-sizing: border-box;
            font-size: small;
        }
        td#space{
            padding: 10px;
            font-size: 14px;
            width: 1000px;
        }
        div#profile{
            margin-top: 90px;
        }
    </style>

</head>
<body>
    @extends('layouts.app')
        @section('content')
        <div>
            <h1 style="font-style: italic;"  >MY PROFILE</h1>
            </div>
            <hr>
            <div id="profile">
                    <form action="" method="POST" id="form">
                <div>
                    <table class="table table-bordered table-hover">
                            <tr align="center">
                                <td colspan="6" class="active" style="background-color: steelblue;"><h2>My Profile</h2></td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;" id="space">User Name</td>
                                <td id="space">
                                    <label> <span>{{$uname}}</span></label>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;" id="space">Full Name</td>
                                <td id="space">
                                <label><span>{{$name}}</label>
                                   
                                </td>
                            </tr>
                                <tr>
                                <td style="font-weight: bold;" id="space">Email Address</td></td>
                                <td>
                                <label><span>{{$email}}</span></label>
                                    
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;" id="space">Gender</td>
                                <td id="space">
                                <label><span>{{$gender}}</span></label>
                                  
                                </td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold;" id="space">Phone Number</td>
                                <td id="space">
                                <label><span>{{$number}}</span></label>
                                    
                                </td>
                            </tr>
                        </tr>
                    </table>
                </div>
                </form>
                <hr>
            </div>
            <div>
                <a href="/edit.profile/{{$id}}">
                <center><button type="button" class="btn btn-info" style="width:250px;" > <span class="glyphicon glyphicon-pencil"></span> Edit Profile</button></center>
                </a>
            </div>
        <hr>
    @endsection
</body>
</html>





