<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateShoppingcartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shoppingcarts', function (Blueprint $table) {
            $table-> id('cartid')->autoIncrement();
            $table->string('productname');
            $table->string('quantity');
            $table->string('price');
            $table->string('tprice');
            $table->binary('image')->nullable();
		    $table->unsignedBigInteger('productid');
            $table->unsignedBigInteger('channelid');
            $table->foreign('productid')->references('id')->on('products');
            $table->foreign('channelid')->references('channelid')->on('channels');
		    $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shoppingcarts');
    }
}
