<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateDeliverisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveris', function (Blueprint $table) {
            $table-> id('deliveriid')->autoIncrement();
            $table->string('username')->unique();
            $table->string('password');
            $table->string('name');
            $table->string('address');
            $table->string('gender');
            $table->string('status');
            $table->binary('image')->nullable();
            $table->string('number');
            $table->string('salary');
            $table->unsignedBigInteger('productid');
            $table->unsignedBigInteger('orderid');
            $table->unsignedBigInteger('adminid');
            $table->foreign('adminid')->references('id')->on('admins');
            $table->foreign('productid')->references('id')->on('products');
            $table->foreign('orderid')->references('orderid')->on('orders');
		    $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveris');
    }
}
