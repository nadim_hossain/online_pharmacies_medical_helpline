<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table-> id('id')->autoIncrement();
            $table->string('channelname');
            $table->string('amount');
            $table->string('bankaccountnumber')->unique();
		    $table->unsignedBigInteger('adminid');
            $table->unsignedBigInteger('channelid');
            $table->foreign('adminid')->references('id')->on('admins');
            $table->foreign('channelid')->references('channelid')->on('channels');
            $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
