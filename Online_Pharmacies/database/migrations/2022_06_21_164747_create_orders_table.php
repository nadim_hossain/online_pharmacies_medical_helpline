<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table-> id('orderid')->autoIncrement();
            $table->string('productname');
            $table->string('quantity');
            $table->string('price');
            $table->string('tprice');
            $table->string('status');
            $table->dateTime('date');
		    $table->unsignedBigInteger('buyerid');
            $table->unsignedBigInteger('productid');
            $table->unsignedBigInteger('channelid');
            $table->unsignedBigInteger('paymentid');
            $table->foreign('buyerid')->references('id')->on('buyers');
            $table->foreign('productid')->references('id')->on('products');
            $table->foreign('channelid')->references('channelid')->on('channels');
            $table->foreign('paymentid')->references('id')->on('payments');
		    $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
