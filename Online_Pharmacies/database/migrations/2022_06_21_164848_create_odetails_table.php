<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateOdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('odetails', function (Blueprint $table) {
            $table-> id('ordersdetailsid')->autoIncrement();
            $table->string('description');
            $table->binary('image')->nullable();
		    $table->unsignedBigInteger('productid');
            $table->unsignedBigInteger('orderid');
            $table->foreign('productid')->references('id')->on('products');
            $table->foreign('orderid')->references('orderid')->on('orders');
		    $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('odetails');
    }
}
