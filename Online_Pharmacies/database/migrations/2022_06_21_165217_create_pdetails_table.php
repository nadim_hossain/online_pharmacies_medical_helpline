<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreatePdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdetails', function (Blueprint $table) {
            $table-> id('pdetailid')->autoIncrement();
            $table->string('productname');
            $table->string('price');
            $table->string('description');
            $table->string('quantity');
            $table->binary('image')->nullable();
		    $table->unsignedBigInteger('productid');
             $table->foreign('productid')->references('id')->on('products');
             $table->dateTime('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
             $table->dateTime('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdetails');
    }
}
