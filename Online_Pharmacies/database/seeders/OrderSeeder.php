<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $mytime = Carbon::today();

        DB::table('orders')->insert([
            'productname' => 'Abacavir',
            'quantity' => '3',
            'price' => '120000',
            'tprice' => '360000',
            'status' => 'Done',
            'date' => $mytime,
            'buyerid' => '1',
            'productid' => '1',
            'channelid' => '1',
            'paymentid' => '1'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Adrenaclick epinephrine',
            'quantity' => '4',
            'price' => '20000',
            'tprice' => '80000',
            'status' => 'Done',
            'date' => $mytime,
            'buyerid' => '3',
            'productid' => '2',
            'channelid' => '1',
            'paymentid' => '1'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Advil (ibuprofen)',
            'quantity' => '5',
            'price' => '100000',
            'tprice' => '500000',
            'status' => 'Done',
            'date' => $mytime,
            'buyerid' => '1',
            'productid' => '3',
            'channelid' => '1',
            'paymentid' => '1'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Elase (fibrinolysin w/dnase-topical ointment)',
            'quantity' => '5',
            'price' => '90000',
            'tprice' => '450000',
            'status' => 'pending',
            'date' => $mytime,
            'buyerid' => '1',
            'productid' => '4',
            'channelid' => '1',
            'paymentid' => '1'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Emtriva emtricitabine',
            'quantity' => '5',
            'price' => '100',
            'tprice' => '500',
            'status' => 'Done',
            'date' => $mytime,
            'buyerid' => '2',
            'productid' => '8',
            'channelid' => '2',
            'paymentid' => '2'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Erleada apalutamide',
            'quantity' => '50',
            'price' => '950',
            'tprice' => '47500',
            'status' => 'Done',
            'date' => $mytime,
            'buyerid' => '1',
            'productid' => '9',
            'channelid' => '2',
            'paymentid' => '2'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Etravirine Intelence',
            'quantity' => '100',
            'price' => '700',
            'tprice' => '70000',
            'status' => 'pending',
            'date' => $mytime,
            'buyerid' => '2',
            'productid' => '10',
            'channelid' => '2',
            'paymentid' => '2'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Ketoconazole vs Selenium Sulfide',
            'quantity' => '100',
            'price' => '150',
            'tprice' => '15000',
            'status' => 'Done',
            'date' => $mytime,
            'buyerid' => '3',
            'productid' => '12',
            'channelid' => '3',
            'paymentid' => '3'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Kytril (granisetron-injection)',
            'quantity' => '80',
            'price' => '600',
            'tprice' => '48000',
            'status' => 'Done',
            'date' => $mytime,
            'buyerid' => '1',
            'productid' => '14',
            'channelid' => '3',
            'paymentid' => '3'
        ]);

        DB::table('orders')->insert([
            'productname' => 'Uni-Ease (docusate-oral)',
            'quantity' => '50',
            'price' => '1000',
            'tprice' => '50000',
            'status' => 'pending',
            'date' => $mytime,
            'buyerid' => '3',
            'productid' => '15',
            'channelid' => '3',
            'paymentid' => '3'
        ]);
    }
}
