<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class pdetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pdetails')->insert([
            'productname' => 'Iphone',
            'price' => '120000',
            'description' => '1',
            'quantity' => '1',
            'image' => '',
            'productid' => '1'

        ]);
    }
}