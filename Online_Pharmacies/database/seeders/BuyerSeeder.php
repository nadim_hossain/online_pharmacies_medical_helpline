<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BuyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buyers')->insert([
            'username' => 'mohok909',
            'password' => 'mohok9o9@@',
            'name' => 'Mohok',
            'address' => 'kuratoli',
            'gender' => 'male',
            'status' => 'active',
            'image' => '',
            'mobile' => '01767268796',
        ]);

        DB::table('buyers')->insert([
            'username' => 'sufian',
            'password' => 'sufian9o9@@',
            'name' => 'Sufian',
            'address' => 'kuratoli',
            'gender' => 'male',
            'status' => 'active',
            'image' => '',
            'mobile' => '01763456780',
        ]);

        DB::table('buyers')->insert([
            'username' => 'saidul',
            'password' => 'saidul9o9@@',
            'name' => 'Saidul',
            'address' => 'kuratoli',
            'gender' => 'male',
            'status' => 'active',
            'image' => '',
            'mobile' => '01741035680',
        ]);

        DB::table('buyers')->insert([
            'username' => 'ratul',
            'password' => 'ratul9o9@@',
            'name' => 'Ratul',
            'address' => 'kuratoli',
            'gender' => 'male',
            'status' => 'active',
            'image' => '',
            'mobile' => '01743862780',
        ]);

        DB::table('buyers')->insert([
            'username' => 'nayeem',
            'password' => 'nayeem9o9@@',
            'name' => 'Nayeem',
            'address' => 'kuratoli',
            'gender' => 'female',
            'status' => 'inactive',
            'image' => '',
            'mobile' => '01862124870',
        ]);
    }
}
