<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DeliveriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deliveris')->insert([
            'username' => 'Sumit',
            'password' => 'Sumit!1155',
            'name' => 'Sumit',
            'address' => 'Kuratoli',
            'gender' => 'female',
            'status' => 'active',
            'image' => '',
            'number' => '01767268796',
            'salary' => '3000',
            'adminid' => '1',
            'productid' => '1',
            'orderid' => '1'
        ]);
    }
}
