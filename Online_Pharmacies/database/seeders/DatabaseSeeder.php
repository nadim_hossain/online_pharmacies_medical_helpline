<?php

namespace Database\Seeders;

use App\Models\Coupon;
use Faker\Provider\ar_EG\Payment;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
            SellerSeeder::class,
            ChannelSeeder::class,
            ProductSeeder::class,
            BuyerSeeder::class,
            PaymentSeeder::class,
            OrderSeeder::class,
            OdetailSeeder::class,
            DeliveriSeeder::class,
            pdetailSeeder::class,
            CartSeeder::class,
            CouponSeed::class
        ]);
    }
}
