<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminLoginController;
use App\Http\Controllers\AdminPagesController;
use App\Http\Controllers\CustomersController;
use App\Http\Controllers\AdminEditProfilrController;
use App\Http\Controllers\AdminAddUsersController;
use App\Http\Controllers\BuyerExportController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AdminAddSellersController;
use App\Http\Controllers\AdminChangePasswordController;
use App\Http\Controllers\AdminProfileController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\SendEmailController;
use App\Http\Controllers\VerifyCodeController;
use App\Http\Controllers\AdminAccountsController;
use App\Http\Controllers\AdminPaymentGetwayController;
use App\Http\Controllers\AdminInventoryController;
use App\Http\Controllers\PaymentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.login');
});

//Login Start

Route::get('/login', [AdminLoginController::class, 'login'])->name('login');
Route::post('/login', [AdminLoginController::class, 'loginsubmit'])->name('login');
Route::get('/logout',[AdminLoginController::class, 'logout'])->name('logout');

//login end

//AdminDashboard

Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');

//AdminDashboard End 

//Admin All Pages

Route::get('/customers', [CustomersController::class, 'index'])->name('customers');
Route::get('/accounts', [AdminAccountsController::class, 'accounts'])->name('accounts');
Route::get('/payments', [AdminPaymentGetwayController::class, 'payments'])->name('payments');
Route::get('/inventory', [AdminInventoryController::class, 'inventory'])->name('inventory');

//Admin All Pages End

//Edit Table

Route::post('/tabledit/action', [CustomersController::class, 'action'])->name('customers.action');
Route::post('/tabledit/action2', [CustomersController::class, 'action2'])->name('seller.action');

//Edit Table End

//edit profire

Route::get('/profile', [AdminProfileController::class, 'profile'])->name('profile');
Route::get('/edit.profile/{id}', [AdminEditProfilrController::class, 'editprofile'])->name('editprofile');
Route::post('/adminEdit',[AdminEditProfilrController::class, 'adminEditSubmitted'])->name('adminEdit');

//edit profile end

//add user

Route::get('/add.user', [AdminAddUsersController::class, 'addusers'])->name('addusers');
Route::post('/add.user', [AdminAddUsersController::class, 'addusersubmit'])->name('addusers');

//add user end

//add seller

Route::get('/add.seller', [AdminAddSellersController::class, 'addseller'])->name('addseller');
Route::post('/add.seller', [AdminAddSellersController::class, 'addsellersubmit'])->name('addseller');

//end add seller

//export data

Route::get('/export.user', [BuyerExportController::class, 'buyerexport'])->name('buyerexport');
Route::get('/export.seller', [BuyerExportController::class, 'sellerexport'])->name('sellerexport');
//end export data

//coupon
Route::get('/discount', [CouponController::class, 'discount'])->name('discount');
Route::post('/discount', [CouponController::class, 'couponsubmit'])->name('discount');
Route::post('/coupon/action', [CouponController::class, 'action3'])->name('coupon.action');

//end coupon


//forgetpass

Route::get('/Forget', [VerifyCodeController::class, 'forgetpassword'])->name('forget');
Route::post('/code/verify', [VerifyCodeController::class, 'code'])->name('code.verify');
Route::post('/change', [VerifyCodeController::class, 'change'])->name('change');
Route::get('/verify', [AdminPagesController::class, 'verify'])->name('verify');
Route::post('/sent.email', [SendEmailController::class, 'index'])->name('email');

//end forgetpass

//change password

Route::get('/change_password', [AdminChangePasswordController::class, 'change_password'])->name('change_password');
Route::post('/changepassword',[AdminChangePasswordController::class, 'changepassword'])->name('changepassword');

//end change password

//payment

Route::get('/payment/clint', [PaymentController::class, 'index'])->name('payment.channel');
Route::post('/payment/clint', [PaymentController::class, 'paymentsubmit'])->name('payment.channel');
Route::get('/export.payment', [BuyerExportController::class, 'paymentexport'])->name('export.payment');
Route::get('/export.payment.view', [BuyerExportController::class, 'paymentexport_view'])->name('export.payment.view');

//end payment